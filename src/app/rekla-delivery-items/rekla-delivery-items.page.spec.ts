import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReklaDeliveryItemsPage } from './rekla-delivery-items.page';

describe('ReklaDeliveryItemsPage', () => {
  let component: ReklaDeliveryItemsPage;
  let fixture: ComponentFixture<ReklaDeliveryItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReklaDeliveryItemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReklaDeliveryItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
