import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicSelectableModule } from 'ionic-selectable';
import { IonicModule } from '@ionic/angular';

import { ReklaDeliveryItemsPage } from './rekla-delivery-items.page';

const routes: Routes = [
  {
    path: '',
    component: ReklaDeliveryItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicSelectableModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReklaDeliveryItemsPage]
})
export class ReklaDeliveryItemsPageModule {}
