import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../services/delivery.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-rekla-delivery-items',
  templateUrl: './rekla-delivery-items.page.html',
  styleUrls: ['./rekla-delivery-items.page.scss'],
})
export class ReklaDeliveryItemsPage implements OnInit {
  category = '';
  $items: Observable<any>;
  order = {
    CustomerName: '',
    Address: '',
    PhoneNumber: '',
    Category: '',
    Landmark: '',
    Items: [ {p: null, quantity: 0, name: '', Unit: ''} ]
  };
  constructor(private route: ActivatedRoute , private service: DeliveryService, public loadingController: LoadingController) {
    this.route.paramMap.subscribe(i => {
        this.category = i.get('category');
        this.order.Category = this.category;
    });
    const auth = JSON.parse(localStorage.getItem('auth'));
    this.order.CustomerName = auth.Name;
    this.order.PhoneNumber = auth.PhoneNumber;
  }

  ngOnInit() {
    this.$items = this.service.getProducts(this.category);
  }

  valuedItem( o: any , p: any ) {
    o.name = p.name;
    o.quantity = p.maxQuantity;
    o.Unit = p.unit;
  }
  validate(o: any , p: any) {

    if ( o.quantity >= p.minQuantity && o.quantity <= p.maxQuantity) {
        return;
      }

    o.quantity = p.maxQuantity;
    alert(`Invalid Quantity, Min value=${p.minQuantity} ,Max value=${p.maxQuantity}`);
  }
  remove( i ) {
    this.order.Items.splice(i, 1);
  }
  add() {
    this.order.Items.push({p: null, quantity: 0 , name: '', Unit: '' } );
  }
  async Order() {
    if (this.order.Address === '') {
      alert('Please Add you address');
      return ;
    } else {
      if (!confirm('Your Delivery address is :' + this.order.Address)) {
        return;
      }
    }
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.order.Items.length; i++ ) {
      if (this.order.Items[i].p == null) {
        alert('Please select product: One or more of produuct is empty!');
        return;
        break;
      }
    }
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 6000
    });
    loading.present();
    this.service.Order(this.order).subscribe(o => {
      loading.dismiss();
      alert(o.message);
      if ( o.s) {
        this.order.Items = [ {p: null, quantity: 0, name: '', Unit: ''} ];
      }
    });

  }

}
