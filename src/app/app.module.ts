import { CityServiceProviderService } from './services/city-service-provider.service';
import { AuthService } from './services/auth.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
//import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutes } from './app-routing.module';

import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { LottieSplashScreen  } from '@ionic-native/lottie-splash-screen/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { Camera } from '@ionic-native/camera/ngx';
// Note we need a separate function as it's required
// by the AOT compiler
export function playerFactory() {
  return player;
}


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    LottieModule.forRoot({ player: playerFactory }),
    IonicModule.forRoot(),
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    IonicSelectableModule
  ],
  providers: [
    AuthService,
    CityServiceProviderService,
    StatusBar,
    LottieSplashScreen,
    SocialSharing,
    AndroidPermissions,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
