import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {

  constructor(private socialSharing: SocialSharing) { }

  ngOnInit() {
  }

  shareWP() {
    this.socialSharing.shareViaWhatsApp('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }
  shareFB() {
    this.socialSharing.shareViaFacebook(
      'Download rekla app and book your service instant. https://bit.ly/2UpliLx'
      , null, 'https://bit.ly/2UpliLx').catch( e => { console.log(e) });
  }
  shareIS() {

    this.socialSharing.shareViaInstagram('Download rekla app and book your service instant. https://bit.ly/2UpliLx', null);
  }
  shareTW() {
    this.socialSharing.shareViaTwitter('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }

}
