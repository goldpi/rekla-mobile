import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.page.html',
  styleUrls: ['./service-details.page.scss'],
})
export class ServiceDetailsPage implements OnInit {
item: any;
  constructor() {
    const i = JSON.parse(localStorage.getItem('item'));
    this.item = JSON.parse(i.Breif);
    localStorage.removeItem('item');

  }

  ngOnInit() {
  }

}
