import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Payload } from '../models/city.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  constructor(private http: HttpClient,private router:Router ) {
    super();
  }

  public getAllService(): Observable<Payload> {
    return this.http.get<Payload>(this.BaseUrl + 'api/Cities')
  }

  public Otp(object ): Observable<any> {
    return this.http.post<any>(this.BaseUrl + 'api/Account/phonelogin',object);
  }

  public VerifyOtp(object ): Observable<any> {
    return this.http.post<any>(this.BaseUrl + 'api/Account/OTPLogin', object).pipe( map(o => {
      localStorage.setItem('auth', JSON.stringify(o) );
      localStorage.setItem('token', 'bearer ' + o.accessToken);
      this.router.navigate(['home']);
    }));
  }
}
