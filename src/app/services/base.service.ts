import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  public BaseUrl =  'http://rekla.in/';//'http://localhost:38723/'; //
  constructor() { }

  public serializeObj(obj) {
    const result = [];
    // tslint:disable-next-line: forin
    for (const property in obj) {
        result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
    }
    return result.join('&');
  }

  public serializeObjArry(obj) {
    const result = [];
    obj.array.forEach(element => {
        // tslint:disable-next-line: forin
        for(const property in element) {
        result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
        }
    });
    return result.join('&');
  }


  get Headers(): HttpHeaders {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders( {Authorization: token});
    return headers;
  }
}
