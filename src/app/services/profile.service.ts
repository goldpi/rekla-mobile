import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService extends BaseService {

  constructor(private http: HttpClient, private router:Router) {
    super();
   }
   getDetails(): Observable<any> {
    const headers = this.Headers;
    return this.http.get(this.BaseUrl + '/api/Account/UserDetalis' , {headers});
    /*.pipe(
    map(i => {
        const data = i;
        if ( data.Image ==null || data.Image == undefined) {
          data.Image = "https://www.gravatar.com/avatar/" + md5(data.EmailId, 'hex');
        }else{
         data.Image= 'http://rekla.in'+data.Image;
       }
      this.userDetails=data;
      return data;
      }));
    */
  }
  
  postDetails(data: any): Observable<any> {
    const headers = this.Headers;
    //const body = this.serializeObj(data);
    return this.http.post<any>(this.BaseUrl + '/api/Update/UserDetalis' , data , {headers}).pipe(map( data => {
      const dataw = JSON.parse(localStorage.getItem('auth'));
      dataw.Name = data.Name ;
      localStorage.setItem('auth', JSON.stringify(dataw));
      this.router.navigate(['/', 'profile']);
      return data;

    }));
  }
  postFeedBack(data: any): Observable<any> {
    const body = this.serializeObj(data);
    return this.http.post<any>(this.BaseUrl + '/api/update/FeedBack', body, {headers: this.Headers});
  }

  postPartner(data: any): Observable<any> {
    const body = this.serializeObj(data);
    return this.http.post(this.BaseUrl + '/api/update/partner' , body, {headers: this.Headers});
  }

  postprofileimage(FromData: FormData): Observable<any> {
    return this.http.post<any>(this.BaseUrl + '/api/user/PostUserImage' , FromData, { headers: this.Headers} );
  }

  postAdharimage(FromData: FormData): Observable<any> {
    const headers = this.Headers;
    return this.http.post<any>(this.BaseUrl + '/api/user/PostAdharImage' , FromData, { headers });
  }

  getItems(): Observable<any> {
    const headers = this.Headers;
    return this.http.get<any>(this.BaseUrl + '/api/ServiceRequests/?mo=1', {headers});
  }
}
