import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CityServiceProviderService extends BaseService {

  constructor(private http: HttpClient, private router: Router ) {
    super();
  }

  getService(ServiceId: number, CityId: number): Observable<any> {
    return this.http.get(this.BaseUrl + 'api/Questioner/byService/' + ServiceId + '/' + CityId);
  }

  saveResult(data: any): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders( {Authorization: token});
    return this.http.post(this.BaseUrl + 'api/ServiceRequests', data, {headers} );
   }

}
