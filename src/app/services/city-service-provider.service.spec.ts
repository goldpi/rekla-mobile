import { TestBed } from '@angular/core/testing';

import { CityServiceProviderService } from './city-service-provider.service';

describe('CityServiceProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CityServiceProviderService = TestBed.get(CityServiceProviderService);
    expect(service).toBeTruthy();
  });
});
