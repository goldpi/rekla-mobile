import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService extends BaseService {
BaseUrl = 'https://rekladelivery20200325054039.azurewebsites.net/api/';
  constructor(private http: HttpClient) { super();}

  public getCategories(): Observable<any> {
    return this.http.get<any>(this.BaseUrl + 'products/Category' );
  }

  public getProducts(category: string): Observable<any> {
    return this.http.get<any>(this.BaseUrl + 'products/Category/' + category );
  }
  public Order(object): Observable<any> {
    const body = this.serializeObj(object)
    return this.http.post(this.BaseUrl + 'order', object )
  }
}
