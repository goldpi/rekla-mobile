import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {

  constructor(private socialSharing: SocialSharing) { }

  ngOnInit() {
  }

  shareWP() {
    this.socialSharing.shareViaWhatsApp('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }
  shareFB() {
    this.socialSharing.shareViaFacebook(
      'Download rekla app and book your service instant. https://bit.ly/2UpliLx'
      , null, 'https://bit.ly/2UpliLx').catch( e => { console.log(e) });
  }
  shareIS() {

    this.socialSharing.shareViaInstagram('Download rekla app and book your service instant. https://bit.ly/2UpliLx', null);
  }
  shareTW() {
    this.socialSharing.shareViaTwitter('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }

}


