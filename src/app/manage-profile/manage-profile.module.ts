import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LottieModule } from 'ngx-lottie';
import { IonicModule } from '@ionic/angular';

import { ManageProfilePage } from './manage-profile.page';

const routes: Routes = [
  {
    path: '',
    component: ManageProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    LottieModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManageProfilePage]
})
export class ManageProfilePageModule {}
