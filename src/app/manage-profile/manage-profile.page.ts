import { ProfileService } from './../services/profile.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder,  FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AnimationOptions } from 'ngx-lottie';
@Component({
  selector: 'app-manage-profile',
  templateUrl: './manage-profile.page.html',
  styleUrls: ['./manage-profile.page.scss'],
})
export class ManageProfilePage implements OnInit {


  options: AnimationOptions = { 
    path: 'https://assets6.lottiefiles.com/datafiles/qq04nAXfKjPV6ju/data.json'
  };
  token: string;
  Data: any ;
  formProfile: FormGroup;
  submitted = false;
  s = false;
  constructor(private profile: ProfileService,
              private formBuilder: FormBuilder,
              private toast: ToastController,
              private socialSharing: SocialSharing ) {
    this.formProfile = this.formBuilder.group({
      Name: ['', Validators.required],
      MobileNo: [
        '',
        [Validators.required, Validators.pattern(/^[0-9]{10}$/)]
      ],
      EmailId: [''],
      Address: ['']
    });

   }

  ngOnInit() {
    this.profile.getDetails().subscribe( d => {
      this.Data = d ;
      this.formProfile.setValue({
        'Name': d.Name,
        'MobileNo' : d.MobileNo,
        'EmailId': d.EmailId,
        'Address': d.Address
      }); 
    });
  }


  get f() {
    return this.formProfile.controls;
  }

  changeImage()
  {
    //this.navCtrl.setRoot('ProfileImagePage')
  }
  public async onSubmit(): Promise<boolean> {
    this.submitted = true;

    if (this.formProfile.invalid) {
      const toast = await this.toast.create({
        message: 'Please enter Valid Number',
        duration: 2000
      });
      toast.present();
      return false;

    }
    this.Data.MobileNo = this.formProfile.value.MobileNo;
    this.Data.Name = this.formProfile.value.Name;
    this.Data.Address = this.formProfile.value.Address;
    this.Data.EmailId = this.formProfile.value.EmailId;
    this.s = true;
    this.save();
    return true;
  }
  async save() {
    this.profile.postDetails(this.Data).subscribe(async data => {
      const toast = await this.toast.create({
        message: 'Profile Saved',
        duration: 2000
      });
      toast.present();
    });
  }

  shareWP() {
    this.socialSharing.shareViaWhatsApp('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }
  shareFB() {
    this.socialSharing.shareViaFacebook(
      'Download rekla app and book your service instant. https://bit.ly/2UpliLx'
      , null, 'https://bit.ly/2UpliLx').catch( e => { console.log(e) });
  }
  shareIS() {

    this.socialSharing.shareViaInstagram('Download rekla app and book your service instant. https://bit.ly/2UpliLx', null);
  }
  shareTW() {
    this.socialSharing.shareViaTwitter('Download rekla app and book your service instant. https://bit.ly/2UpliLx');
  }
}
