import { Router } from '@angular/router';
import { ProfileService } from './../services/profile.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  public items: Array<any> = [];
  constructor(private profile: ProfileService, private router: Router) {

  }

  ngOnInit() {
    this.profile.getItems().subscribe( d => { this.items = d; });
  }

  details(item) {
    this.selectedItem = item;
    localStorage.setItem('item', JSON.stringify(item));
    this.router.navigate(['/', 'details']);
  }
}
