import { Camera } from '@ionic-native/camera/ngx';
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../services/delivery.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.page.html',
  styleUrls: ['./medicine.page.scss'],
})
export class MedicinePage implements OnInit {
  base64Image = '';

  order = {
    CustomerName: '',
    Address: '',
    PhoneNumber: '',
    Category: 'Medical',
    Landmark: '',
    Items: [ { quantity: 60, name: '', Unit: 'Tab'} ],
    Image: null
  };
  constructor( private service: DeliveryService, private camera: Camera, public loadingController: LoadingController) { }

  ngOnInit() {
    const auth = JSON.parse(localStorage.getItem('auth'));
    this.order.CustomerName = auth.Name;
    this.order.PhoneNumber = auth.PhoneNumber;
  }

  valuedItem( o: any , p: any ) {
    o.name = p.name;
    o.quantity = p.maxQuantity;
    o.Unit = p.unit;
  }
  validate(o: any) {

    if ( o.quantity >= 0 && o.quantity <= 100) {
        return;
      }

    o.quantity = 60;
    o.Unit = 'Tab' ;
    alert(`Invalid Quantity, Min value=1 ,Max value=100`);
  }
  remove( i ) {
    this.order.Items.splice(i, 1);
  }
  add() {
    this.order.Items.push({ quantity: 60 , name: '', Unit: 'Tab' } );
  }
  async Order() {
    if (this.order.Address === '') {
      alert('Please Add you address');
      return ;
    } else {
      if (!confirm('Your Delivery address is :' + this.order.Address)) {
        return;
      }
    }
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0 ; i < this.order.Items.length; i++ ) {
      if ( this.order.Items[i].name === '' || this.order.Items[i].quantity < 1 || this.order.Items[i].Unit === '') {
        alert('Please select product: One or more of produuct information is empty!');
        return;
        break;
      }
    }
    this.order.Image = this.base64Image;
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 6000
    });
    loading.present();
    this.service.Order(this.order).subscribe(o => {
      loading.dismiss();
      alert(o.message);
      if ( o.s) {
        this.order.Items = [ {quantity: 60, name: '', Unit: 'Tab'} ];
      }
    });

  }
  AccessCamera() {
    this.camera.getPicture({
    targetWidth: 512,
    targetHeight: 512,
    correctOrientation: true,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL
      }).then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
       // this.picture = imageData;
            }, (err) => {
        console.log(err);
      });

   }

   AccessGallery() {
    this.camera.getPicture({
       sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
       destinationType: this.camera.DestinationType.DATA_URL
      }).then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
       // this.picture = imageData;
           }, (err) => {
        console.log(err);
      });
   }

   removeImage() {
     this.base64Image = '';
   }
}
