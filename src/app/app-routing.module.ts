import { AuthguardGuard } from './authguard.guard';
// import { NgModule } from '@angular/core';
import {  Routes } from '@angular/router';

export const AppRoutes: Routes = [

  
  {
    path: 'login',
    loadChildren: () => import('./register/register.module').then(o => o.RegisterPageModule)
  },
  {
    path: 'verify-otp',
    loadChildren: () => import('./verify-otp/verify-otp.module').then(o => o.VerifyOtpPageModule)
  },

  { path: '', loadChildren: () => import('./menu/menu.module').then(o => o.MenuPageModule),
  canActivate: [AuthguardGuard]
}
];

// @NgModule({
//   imports: [
//     RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
//   ],
//   exports: [RouterModule]
// })
// export class AppRoutingModule {}
