import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReklaDeliveryCategoryPage } from './rekla-delivery-category.page';

const routes: Routes = [
  {
    path: '',
    component: ReklaDeliveryCategoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReklaDeliveryCategoryPage]
})
export class ReklaDeliveryCategoryPageModule {}
