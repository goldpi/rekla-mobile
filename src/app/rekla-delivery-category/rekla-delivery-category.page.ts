import { Observable } from 'rxjs';
import { DeliveryService } from './../services/delivery.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rekla-delivery-category',
  templateUrl: './rekla-delivery-category.page.html',
  styleUrls: ['./rekla-delivery-category.page.scss'],
})
export class ReklaDeliveryCategoryPage implements OnInit {

  $services: Observable<any>;
  constructor(private service: DeliveryService) { }

  ngOnInit() {
    this.$services = this.service.getCategories();
  }

}
