import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate  {

  constructor(private routes: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    const token = JSON.parse(localStorage.getItem('auth'));
    const helper = new JwtHelperService();
    if ( token !== undefined && token != null) {
      const isExpired = helper.isTokenExpired(token.accessToken);
      if ( !isExpired ) {
        return true;
      }
    }
    localStorage.clear();
    this.routes.navigate(['login']);
    return false;
  }

}
