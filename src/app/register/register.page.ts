import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {  ToastController } from '@ionic/angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {
  loginFrom: FormGroup;
  submitted = false;
  s = false;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toast: ToastController,
    private router: Router

  ) {


  //  localStorage.clear();
    this.loginFrom = this.formBuilder.group({
      Name: ['', Validators.required],
      PhoneNumber: ['',[Validators.required, Validators.pattern(/\d{10}/)]]
    });
  }
  ngOnInit() {}
  get f() {
    return this.loginFrom.controls;
  }
  public async onSubmit(): Promise<boolean> {
    this.submitted = true;

    if (this.loginFrom.invalid) {
      const toast = await this.toast.create({
        message: 'Please enter Valid Number',
        duration: 2000
      });
      toast.present();
      return false;

    }

    this.s = true;
    // Post
    this.auth
      .Otp({
        Name: this.loginFrom.value.Name,
        PhoneNumber: this.loginFrom.value.PhoneNumber
      })
      .subscribe(async data => {
        const toast = await this.toast.create({
          message: 'OTP sent!',
          duration: 2000
        });
        toast.present();
       // alert(JSON.stringify(data))

        await this.requestOtp();
      },
      async error => {
        const toast = await this.toast.create({
          message: 'OTP sent!',
          duration: 2000
        });
        toast.present();
       // alert(JSON.stringify(error))
      });
    return true;
  }
  async requestOtp() {
    localStorage.setItem(
      'otp',
      JSON.stringify({
        Name: this.loginFrom.value.Name,
        PhoneNumber: this.loginFrom.value.PhoneNumber
      })
    );
    this.router.navigate(['verify-otp']);
  }
}
