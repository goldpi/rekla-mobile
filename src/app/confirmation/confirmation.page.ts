import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.page.html',
  styleUrls: ['./confirmation.page.scss'],
})
export class ConfirmationPage implements OnInit {
  options: AnimationOptions = { 
    path: 'https://assets4.lottiefiles.com/datafiles/s2s8nJzgDOVLOcz/data.json'
  };
  constructor() { }

  ngOnInit() {
  }

}
