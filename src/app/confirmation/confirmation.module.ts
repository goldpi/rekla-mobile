import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LottieModule } from 'ngx-lottie';
import { IonicModule } from '@ionic/angular';

import { ConfirmationPage } from './confirmation.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LottieModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmationPage]
})
export class ConfirmationPageModule {}
