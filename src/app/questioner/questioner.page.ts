import { FormBuilder } from '@angular/forms';
import { CityServiceProviderService } from './../services/city-service-provider.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
@Component({
  selector: 'app-questioner',
  templateUrl: './questioner.page.html',
  styleUrls: ['./questioner.page.scss']
})
export class QuestionerPage implements OnInit {
  city: any = null;
  service: any = null;

  userForm: FormGroup;
  serviceId: any;
  data: any;
  Questioner = [];
  hasQuestion: any;
  Index = 0;
  selctedQuestion: any;
  selectedAnswer = {
    Answer: [],
    Query: '',
    Index: 1
  };
  Timings = [
    '6 AM-7 AM',
    '7 AM-8 AM',
    '8 AM-9 AM',
    '9 AM-10 AM',
    '10 AM-11 AM',
    '11 AM-12 PM',
    '12 PM-1 PM',
    '1 PM-2 PM',
    '2 PM-3 PM',
    '3 PM-4 PM',
    '4 PM-5 PM',
    '5 PM-6 PM',
    '6 PM-7 PM',
    '7 PM-8 PM'
  ];
  answers = [];
  answersIndex = 0;
  Details = false;
  AnswerQuestion = false;
  Splash = true;
  City: any;
  posting = false;
  minDate: string;
  maxDate: string;
  Area: any;
  cityId: any;
  submitted = false;
  constructor(private router: Router,
              private fm: FormBuilder ,
              private route: ActivatedRoute ,
              private serviceProvider: CityServiceProviderService) {
        this.minDate = moment.utc().startOf('day').format('YYYY-MM-DD');
        this.maxDate = moment.utc().add(10, 'day').format('YYYY-MM-DD');
        this.route.paramMap.subscribe(s => {
        console.log(s);
        this.serviceId = s.get('sid');
        this.cityId = s.get('cid');
        this.init();
    });

  }

  init(){
    this.serviceProvider.getService(this.serviceId, this.cityId)
    .subscribe(data => {

      this.data = data.data;
      this.City = data.City;
      this.city = data.City;
      this.Area = data.area;
      this.hasQuestion = data.dataStatus;
      this.data.Service = data.service;
      this.service = data.service;
      this.Questioner = JSON.parse(data.data.Questioner);
      this.selctedQuestion = this.Questioner.length > 0 ? this.Questioner[0] : null;
      this.pushAnswer(this.selctedQuestion.Order, this.selctedQuestion.Query);

      });
    this.userForm = this.fm.group({
      Name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z][a-zA-Z ]+/)]),
      ContactNo: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{10,10}$/)]),
      Email: new FormControl(''),
      Address: new FormControl('', Validators.required),
      OnDate: new FormControl('', Validators.required),
      OnTime: new FormControl([], Validators.required),
      Area: new FormControl([], Validators.required)
    });
  }
  ngOnInit() {

  }
  Start() {
    this.Splash = false;
    this.AnswerQuestion = true;
  }
  Next() {
    if (this.selctedQuestion.IsLastQuestion) {
      this.AnswerQuestion = false;
      this.Details = true;
      return false;
    }
    if (this.answers[this.answersIndex].Answer.length > 0) {

      if (this.selctedQuestion.Type === 'Radio') {
          this.QuestionWasRadio();
      } else {
          this.QuestionIsNotRadio();
      }
    } else {
      if (this.selctedQuestion.Type === 'Text' && !this.selctedQuestion.Required) {
        this.NextQuestion();
        return;
      }
    }


  }

QuestionIsNotRadio() {
    if (!this.NextQuestion()) {
        this.AnswerQuestion = false;
        this.Details = true;
    }
}

QuestionWasRadio() {
    const SelectOption = this.selctedQuestion.Option.filter(i => i.Text === this.answers[this.answersIndex].Answer[0])[0];
    console.log(SelectOption);
    if (SelectOption.JumpToQuestion) {
        this.JumpToQuestion(SelectOption);
    } else {
        this.NextQuestion();
    }
}

NextQuestion() {

  if (this.Questioner.length > this.Index + 1) {
    this.selctedQuestion = this.Questioner[this.Index + 1];
    this.Index++;
    this.pushAnswer(this.selctedQuestion.Order, this.selctedQuestion.Query);
    return true;
  }
  return false;
}

   JumpToQuestion(SelectOption: any) {
        const nextQueston = this.Questioner.findIndex(i => i.Order === SelectOption.Qid);
        this.selctedQuestion = this.Questioner[nextQueston];
        this.Index = nextQueston;
        this.pushAnswer(this.selctedQuestion.Order, this.selctedQuestion.Query);
    }

  addToAnswer() {

  }


  answerRadio(text) {
    this.deleteExtra();
    if (this.answers[this.answersIndex].Answer.length === 1) {
    this.answers[this.answersIndex].Answer.pop();
    }
    this.answers[this.answersIndex].Answer.push(text);
  }

  deleteExtra() {
      if (this.answers.length !== (this.answersIndex + 1)) {
          const count = this.answers.length - (this.answersIndex + 1);
          this.answers.splice(this.answersIndex + 1, count);
      }
  }

  answerCheckBox(text) {
    this.deleteExtra();

    if (this.answers[this.answersIndex].Answer.filter(i => i === text).length > 0) {
      const index = this.answers[this.answersIndex].Answer.findIndex(i => i === text);
      this.answers[this.answersIndex].Answer.splice(index, 1);
    } else {
      this.answers[this.answersIndex].Answer.push(text);
    }

  }

  pushAnswer(index: number, Query: string) {
    if (this.answers.filter(i => i.Index === index).length === 0) {
      this.answers.push({
        Answer: [],
        Query,
        Index: index
      });
    }
    this.answersIndex = this.answers.findIndex(i => i.Index === index);
  }

  Previous() {
    this.Index = this.Questioner.findIndex(i => i.Order === this.answers[this.answersIndex - 1].Index);
    this.selctedQuestion = this.Questioner[this.Index];
    this.answersIndex--;
  }



  isChecked(text) {
    if (this.answers[this.answersIndex].Answer.filter(i => i === text).length > 0) {
      return true;
    } else {
      return false;
    }
  }
  TextChange($event: any, index: any, text: any) {
    this.answers[this.answersIndex].Answer[index] = text + ' : ' + $event.target.value;
    console.log($event);
  }


  public closeAlert() {

  }
  MakeRequest() {
    this.submitted = true;
    if (!this.userForm.valid) {
     // UIHelper.AlertError("Please fill the form correctly", "Error", this.alerts)
    } else {

      const serviceData = JSON.stringify( {
        Answers: this.answers,
        FormDetails: this.userForm.value,
        Service: this.data.Service.Name
      });
      const tu = this.userForm.value.OnDate + ' Time:' + this.userForm.value.OnTime;
      if (tu == null) {
       return;
      }
      const service = {
        Name: this.userForm.value.Name,
        Email: this.userForm.value.Email,
        MobileNo: this.userForm.value.ContactNo,
        Address: this.userForm.value.Address,
        Area: this.userForm.value.Area,
        City: this.City.Name,
        Breif: serviceData,
        ServiceId: this.data.Service.ServiceId ,
        DateOfService: tu
        };

      this.serviceProvider.saveResult(service).subscribe(data => {
        this.router.navigate(['/','confirmation']);
      }, error => {
      });

    }
  }
  addDays(date: Date, days: number): Date {
    console.log('adding ' + days + ' days');
    console.log(date);
    date.setDate(date.getDate() + days);
    console.log(date);
    return date;
  }

  back() {
    this.Details = false;
    this.AnswerQuestion = true;
  }

  get f() {
    return this.userForm.controls;
  }
}
