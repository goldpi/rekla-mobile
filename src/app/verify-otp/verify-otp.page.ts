
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ToastController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.page.html',
  styleUrls: ['./verify-otp.page.scss'],
})
export class VerifyOtpPage implements OnInit {

  object: { Name: any; PhoneNumber: any; Code: any };
  public OTP: string;
  otpForm: FormGroup;
  submitted = false;
  constructor( private auth: AuthService,
    private toast: ToastController,
    private router: Router,
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController) {
      this.object = JSON.parse(localStorage.getItem('otp'));
      this.otpForm = this.formBuilder.group({
     OTP: ['', [Validators.required]]
   });
     }

  ngOnInit() {
  }

  async verifyOtp() {
    this.submitted = true;
    if (this.otpForm.invalid) {
      return false;
    }
    this.object.Code = this.otpForm.value.OTP;
    this.auth.VerifyOtp(this.object).subscribe(p => {
      localStorage.removeItem('otp');
      this.router.navigate(['home']);
    });
  }

  get f() {
    return this.otpForm.controls;
  }

  async resendOtp() {
    this.auth.Otp(this.object).subscribe(async data => {
      const toast = await this.toast.create({
        message: 'OTP sent!',
        duration: 2000
      });
      toast.present();
    });
  }
}
