import { OnInit, Component } from '@angular/core';
import { Events, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'menu.page.html',
  styleUrls: ['menu.page.scss']
})

export class MenuPage implements OnInit {
  name: string;
  constructor(
    // private router: Router,
    public event: Events,
    // public storage: Storage,
    public menuCtrl: MenuController,
    private router: Router
  ) {}

  ngOnInit() {
   const auth = JSON.parse(localStorage.getItem('auth'));
   this.name = auth.Name;
  }

  OpenLink(url: any) {
    this.menuCtrl.close();
    this.router.navigate(url);
  }
}
