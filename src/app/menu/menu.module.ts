import { ReklaDeliveryCategoryPageModule } from './../rekla-delivery-category/rekla-delivery-category.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';
import { AuthguardGuard } from '../authguard.guard';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: MenuPage,
        children: [
          { path: '', loadChildren: () => import ('./../home/home.module').then( m => m.HomePageModule),
          canActivate: [AuthguardGuard] },
          {
            path: 'home',
            loadChildren: () => import('./../home/home.module').then(m => m.HomePageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'service/:sid/:cid',
            loadChildren: () => import('../questioner/questioner.module').then(m => m.QuestionerPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'confirmation',
            loadChildren: () => import('../confirmation/confirmation.module').then(m => m.ConfirmationPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'profile',
            loadChildren: () => import('../manage-profile/manage-profile.module').then(m => m.ManageProfilePageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'list',
            loadChildren: () => import('../list/list.module').then(m => m.ListPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'details',
            loadChildren: () => import('../service-details/service-details.module').then(m => m.ServiceDetailsPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'about-us',
            loadChildren: () => import('../about/about.module').then(m => m.AboutPageModule),
            canActivate: [AuthguardGuard]
          }
          ,
          {
            path: 'privacy',
            loadChildren: () => import('../privacy/privacy.module').then(m => m.PrivacyPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'support',
            loadChildren: () => import('../support/support.module').then(m => m.SupportPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'category',
            loadChildren: () => import('../rekla-delivery-category/rekla-delivery-category.module')
            .then(m => m.ReklaDeliveryCategoryPageModule),
            canActivate: [AuthguardGuard]
          },
          {
            path: 'category/:category',
            loadChildren: () => import('../rekla-delivery-items/rekla-delivery-items.module')
            .then(m => m.ReklaDeliveryItemsPageModule),
            canActivate: [AuthguardGuard]
          }
          ,
          {
            path: 'category/Medical/page',
            loadChildren: () => import('../medicine/medicine.module')
            .then(m => m.MedicinePageModule),
            canActivate: [AuthguardGuard]
          }
        ]
      }
    ])
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
