export interface Payload {
    Cities?: City[];
    Services?: FserviceElement[];
    Fservice?: FserviceElement[];
}

export interface City {
    CityId?: number;
    Active?: boolean;
    ImageUrl?: null;
    Name?: string;
    Services?: Service[];
}

export interface Service {
    Name?: string;
    ServiceId?: number;
    ShortName?: string;
    Description?: null | string;
}

export interface FserviceElement {
    Name?: string;
    ServiceId?: number;
    ImageUrl?: string;
    FeaturedImageUrl?: string;
    ShortName?: string;
    Description?: null | string;
}
