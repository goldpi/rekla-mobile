import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit  {
  cities = [];
  Services = [];
  slides = [];
  cityselected = null;
  selectedService = null;
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.cityselected = JSON.parse(localStorage.getItem('city'));
    this.auth.getAllService().subscribe(data => {
      this.cities = data.Cities;
      this.Services = data.Services;
      this.slides = data.Fservice;
    });
  }
  selectService(Service) {
    if (this.cityselected !== null) {
    localStorage.setItem('city', JSON.stringify(this.cityselected));
    this.selectedService = Service ;
    this.router.navigate(['/', 'service', '' + Service.ServiceId + '', '' + this.cityselected.CityId + '']);
    }
  }

  isIndependanceDay(): boolean {
    return  false; //moment.utc().startOf('day').format('MM-DD')=='08-15';
  }
  isRepublicDay(): boolean {
  return false; // moment.utc().startOf('day').format('MM-DD')=='01-26';
  }

  imagePath(data: string) {
    return 'http://rekla.in/' + data;
  }


}
